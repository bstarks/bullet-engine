﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

public static class Vector2Extensions
{
    public static float SignedAngle(Vector2 from, Vector2 to)
    {
        float rad1 = Mathf.Atan2(from.y, from.x);
        float rad2 = Mathf.Atan2(to.y, to.x);
        return (rad2 - rad1) * Mathf.Rad2Deg;
    }
}

