﻿using System;
using UnityEngine;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Rationals
{
    public class Rational
    {
        private int numerator;
        private int denominator;

        public Rational(int numerator) : this(numerator, 1) { }

        public Rational(int numerator, int denominator)
        {
            this.numerator = numerator;
            this.denominator = denominator;
        }

        public static Rational operator +(Rational r1, Rational r2)
        {
            int commonDenom = r1.denominator * r2.denominator;
            int newNum = (r1.numerator * r2.denominator) + (r2.numerator * r1.denominator);
            return new Rational(newNum, commonDenom);
        }

        public static Rational operator -(Rational r1, Rational r2)
        {
            int commonDenom = r1.denominator * r2.denominator;
            int newNum = (r1.numerator * r2.denominator) - (r2.numerator * r1.denominator);
            return new Rational(newNum, commonDenom);
        }

        public static Rational operator *(Rational r1, Rational r2)
        {
            return new Rational(r1.numerator * r2.numerator, r1.denominator * r2.denominator);
        }

        public static Rational operator /(Rational r1, Rational r2)
        {
            return r1 * new Rational(r2.denominator, r2.numerator);
        }

        public static explicit operator int(Rational r)
        {
            return (int) Math.Ceiling((decimal) (r.numerator / r.denominator));
        }

        public static implicit operator Rational(int i)
        {
            return new Rational(i);
        }

        public static explicit operator float(Rational r)
        {
            return (r.numerator / r.denominator);
        }


        public override string ToString()
        {
            int intRep = numerator / denominator;
            if (intRep * denominator == numerator)
            {
                return intRep.ToString();
            }
            return numerator + "/" + denominator;
        }
    }
}
