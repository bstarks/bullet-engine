﻿using UnityEngine;
using Rationals;
using System.Collections;

[RequireComponent(typeof(AudioSource))]
public class AudioLooper : MonoBehaviour {
    public int beatsPerMinute;
    public int beatsPerMeasure;
    public int startMeasure;
    public int endMeasure;


    private AudioSource audioSource;
    private Rational samplesPerSecond;
    private Rational samplesPerMeasure;
    private Rational startPositionInPCMSamples;
    private Rational endPositionInPCMSamples;



    // Use this for initialization
    void Start () {
        audioSource = GetComponent<AudioSource>();
        Reinit();
	}
	
	// Update is called once per frame
	void Update () {
	    if (audioSource.timeSamples >= (int) endPositionInPCMSamples)
        {
            audioSource.timeSamples = (int) startPositionInPCMSamples;
        }
	}

    void Reinit()
    {
        SyncToClip();
        Rational secondsPerMinute = 60;
        Rational minutesPerBeat = new Rational(1, beatsPerMinute);
        Rational secondsPerBeat = secondsPerMinute * minutesPerBeat;
        Rational samplesPerBeat = samplesPerSecond * secondsPerBeat;
        samplesPerMeasure = samplesPerBeat * beatsPerMeasure;
        startPositionInPCMSamples = (startMeasure - 1) * samplesPerMeasure;
        endPositionInPCMSamples = (endMeasure - 1) * samplesPerMeasure;
        Debug.Log("Going from " + startPositionInPCMSamples + " to " + endPositionInPCMSamples);
        Debug.Log("Sample rate is " + audioSource.clip.frequency);
    }

    private void SyncToClip()
    {
        samplesPerSecond = audioSource.clip.frequency;
    }
}

