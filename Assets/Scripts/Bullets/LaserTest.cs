﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LaserTest : MonoBehaviour {

    public float timeStep = .25f;
    public float warmupWait = 1f;
    public GameObject prefabLaser;
    public int laserLength = 50;

    private float elapsedTime = 0f;
    private int lasersCreated = 0;
    private bool canFire;
    private float angle = 0;

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        elapsedTime += Time.deltaTime;
        if (!canFire && elapsedTime < timeStep)
        {
            return;
        }
        else if (!canFire)
        {
            canFire = true;
        }
        else if (elapsedTime >= timeStep && lasersCreated <= laserLength)
        {
            GameObject laserBullet = Instantiate(prefabLaser, transform);
            laserBullet.GetComponent<BulletComponent>().angle = angle;
            lasersCreated += 1;
        }
        else if (lasersCreated > laserLength)
        {
            lasersCreated = 0;
            canFire = false;
            elapsedTime = 0;
            angle += 33f;
        }
	}
}
