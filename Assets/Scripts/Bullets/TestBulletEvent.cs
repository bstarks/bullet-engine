﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestBulletEvent : IBulletEvent
{
    private float elapsedSeconds = 0;
    private const float timeToFire = 0.016f;
    private const float angleDiff = 5f;
    Vector2 center;
    float angle;
    Sprite sprite;
    int index;

    private List<GameObject> activeBullets;
    private BulletCacher bulletCacher;
    private Scene scene;

    public TestBulletEvent(Scene scene)
    {
        activeBullets = new List<GameObject>();
        center = scene.intoPlayArea(new Vector2(scene.getPlayAreaWidth() / 2, - (scene.getPlayAreaHeight() / 2  - 70)));
        angle = 0;
        sprite = Resources.Load<Sprite>("Sprites/greenBullet");
        this.bulletCacher = scene.getBulletCacher();
        bulletCacher.preallocate(500, "Green Bullet");
        this.scene = scene;
    }

    public GameObject createBullet(Vector2 position, float angle, Sprite sprite, int i)
    {
        float randomness = 0;// UnityEngine.Random.Range(-10, 10);
        GameObject bulletObj = bulletCacher.getBullet("Green Bullet");
        BulletComponent bullet = bulletObj.GetComponent<BulletComponent>();
        bullet.reset(position, angle + randomness , -330 - randomness , 220, sprite);
        bullet.maxSpeed = 250;
        return bulletObj;
    }

    public Boolean outOfBounds(GameObject bulletObj)
    {
        BulletComponent bullet = bulletObj.GetComponent<BulletComponent>();
        return bullet.speed == bullet.maxSpeed &&
            scene.onScene(bullet.position, 8);
    }

    public void update(float dt)
    {
        activeBullets.ForEach(bullet =>
        {
            if (outOfBounds(bullet))
            {
                activeBullets.Remove(bullet);
                bulletCacher.cacheBullet(bullet, "Green Bullet");
            }
        });
        elapsedSeconds += dt;
        if (elapsedSeconds > timeToFire)
        {
            elapsedSeconds -= timeToFire;
            GameObject bullet = createBullet(center, angle, sprite, index);
            index++;
            angle = (angle + angleDiff) % 360;
            activeBullets.Add(bullet);
            bullet = createBullet(center, angle, sprite, index);
            index++;
            angle = (angle + angleDiff) % 360;
            activeBullets.Add(bullet);
        }
    }
}

