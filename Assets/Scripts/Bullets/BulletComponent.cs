﻿using UnityEngine;

[RequireComponent(typeof(SpriteRenderer))]
[RequireComponent(typeof(Collider2D))]
public class BulletComponent : MonoBehaviour
{

    public Vector2 position;
    public float speed;
    public float maxSpeed = Mathf.Infinity;
    public float minSpeed = Mathf.NegativeInfinity;
    public float acceleration;

    /// <summary>
    ///  Direction bullet is facing in degrees relative to the y-axis
    ///  (0 points down, 90 points right)
    /// </summary>
    public float angle
    {
        set
        {
            if (value != currentAngle)
            {
                currentAngle = value;
                direction = Quaternion.Euler(Vector3.forward * value) * Vector2.down;
                gameObject.transform.rotation = Quaternion.AngleAxis(value, Vector3.forward);
            }
        }
        get
        {
            return currentAngle;
        }
    }

    private Vector2 direction = Vector2.down;
    private float currentAngle = 0;

    

    private new SpriteRenderer renderer;
    private new Collider2D collider;


    void Start()
    {
        renderer = gameObject.GetComponent<SpriteRenderer>();
        collider = gameObject.GetComponent<Collider2D>();
    }

    void Update()
    {
        updatePosition(Time.deltaTime);
    }

    public void reset(Vector2 position, float angle, float speed, Sprite sprite)
    {
        reset(position, angle, speed, 0, sprite);
    }

    public void reset(Vector2 position, float angle, float speed, float acceleration, Sprite sprite)
    {
        this.position = position;
        this.angle = angle;
        this.speed = speed;
        this.acceleration = acceleration;

        gameObject.transform.position = position;
        gameObject.transform.rotation = Quaternion.AngleAxis(Vector2Extensions.SignedAngle(Vector2.down, direction), Vector3.forward);

        renderer = gameObject.GetComponent<SpriteRenderer>();
        renderer.sprite = sprite;
    }

    private void updatePosition(float dt)
    {
        var dSpeed = speed * dt;
        position += dSpeed * direction;

        var dAccel = acceleration * dt;
        speed += dAccel;

        speed = Mathf.Clamp(speed, minSpeed, maxSpeed);

        gameObject.transform.position = position;
    }

    public void hide()
    {
        renderer.enabled = false;
    }

    public void show()
    {
        renderer.enabled = true;
    }
}


