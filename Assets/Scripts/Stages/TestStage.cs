﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestStage : Stage
{
    IBulletEvent bulletEvent;

    public TestStage(Scene scene)
    {
        bulletEvent = new TestBulletEvent(scene);
    }

    public void update(float dt)
    {
        bulletEvent.update(dt);
    }
}