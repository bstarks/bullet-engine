﻿using UnityEngine;
using System.Collections;

/*
Represents a 'level'
The stage will be attached to a scene and
should be used to control the flow of the game
*/
public interface Stage
{
    void update(float dt);
}
