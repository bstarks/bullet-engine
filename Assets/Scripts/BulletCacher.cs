﻿using System.Collections.Generic;
using System;
using UnityEngine;

/// <summary>
/// Class for caching bullets. Bullets are keyed by prefab name.
/// </summary>
public class BulletCacher {
    private Dictionary<String, Queue<GameObject>> bulletCache;


    public BulletCacher()
    {
        bulletCache = new Dictionary<String, Queue<GameObject>>();
    }

    /// <summary>
    /// Add a bullet into the cache
    /// </summary>
    /// <param name="bullet">GameObject corresponding to bullet</param>
    /// <param name="key">Name of bullet prefab (used as key in cache)</param>
    public void cacheBullet(GameObject bullet, String key)
    {
        bullet.SetActive(false);
        if (!bulletCache.ContainsKey(key))
        {
            bulletCache.Add(key, new Queue<GameObject>());
        }
        bulletCache[key].Enqueue(bullet);
    }

    /// <summary>
    /// Take a bullet from the cache
    /// </summary>
    /// <param name="key">Name of bullet prefab</param>
    /// <returns>Instance of bullet prefab</returns>
    public GameObject getBullet(String key)
    {
        GameObject bullet;
        if (!bulletCache.ContainsKey(key))
        {
            bullet = instantiateBullet(key);
        }
        else if (bulletCache[key].Count == 0)
        {
            bullet = instantiateBullet(key);
        }
        else {
            bullet = bulletCache[key].Dequeue();
        }
        //This probably isn't null
        bullet.SetActive(true);
        return bullet;
    }

    /// <summary>
    /// Instantiate bullets ahead of time and add them to the cache
    /// </summary>
    /// <param name="howMany">Number of bullets to instantiate</param>
    /// <param name="key">Name of bullet prefab</param>
    public void preallocate(int howMany, String key)
    {
        Queue<GameObject> bullets = new Queue<GameObject>();
        for (int i = 0; i < howMany; i++)
        {
            bullets.Enqueue(instantiateBullet(key));
        }
        bulletCache[key] = bullets;
    }

    private GameObject instantiateBullet(String key)
    {
        GameObject bullet = GameObject.Instantiate(Resources.Load("Prefabs/" + key)) as GameObject;
        bullet.SetActive(false);
        return bullet;
    }
}
