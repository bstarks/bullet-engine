﻿using UnityEngine;
using System.Collections;
using System;

/*
The scene that a stage runs within.
Once a stage is attached to the scene, the scene acts as a 'library' of sorts
for carrying out stage actions
This class is a facade of sorts, to reduce the amount of direct interaction between
a stage's logic and Unity
*/
[RequireComponent(typeof(SpriteRenderer))]
public class Scene : MonoBehaviour {
    public Sprite overlay;
    public int playAreaTopLeftx, playAreaTopLefty;
    public int playAreaBottomRightx, playAreaBottomRighty;

    private Stage attachedStage;
    private BulletCacher bulletCacher;
    private new SpriteRenderer renderer;

    // Use this for initialization
    void Start () {
        transform.position = Vector2.zero;
        renderer = gameObject.GetComponent<SpriteRenderer>();
        renderer.sprite = overlay;

        //Initialize scene here
        bulletCacher = new BulletCacher();
        attachedStage = new TestStage(this);

        Debug.Log("Width is " + getPlayAreaWidth());
        Debug.Log("Height is " + getPlayAreaHeight());
	}
	
	// Update is called once per frame
	void Update () {
        attachedStage.update(Time.deltaTime);
	}

    public int getPlayAreaWidth()
    {
        return playAreaBottomRightx - playAreaTopLeftx;
    }

    public int getPlayAreaHeight()
    {
        return  playAreaTopLefty - playAreaBottomRighty;
    }

    public bool onScene(Vector2 position, int radius)
    {
        return (position.x + radius < playAreaTopLeftx || position.x - radius > playAreaBottomRightx)
            || (position.y + radius < playAreaBottomRighty || position.y - radius > playAreaTopLefty);
    }

    public BulletCacher getBulletCacher()
    {
        return bulletCacher;
    }

    // Offsets the input vector by the top left vector of the play area and returns the result
    public Vector2 intoPlayArea(Vector2 vector)
    {
        return new Vector2(vector.x + playAreaTopLeftx, vector.y + playAreaTopLefty);
    }
}
